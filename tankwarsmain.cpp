//kelvin silva
//game programming all in one by jonathan harbour project

#include "tankwar.h"
#include <allegro.h>

int main()
{
    allegro_init();
    install_keyboard();
    install_timer();
    srand(time(NULL));
    setupscreen();
    setupdebris();
    setuptanks();
    //game loop
    while (!gameover){

        //erasetank
        erasetank(0);
        erasetank(1);

        //collision check
        clearpath(0);
        clearpath(1);
        //movetanks
        movetank(0);
        movetank(1);
        //draw tank pos move
        drawtank(0);
        drawtank(1);
        //updatebullet
        updatebullet(0);
        updatebullet(1);
        //keypress
        if (keypressed())
        getinput();
        //slow down game
        rest(40);
    }
    allegro_exit();
    return 0;

    }END_OF_MAIN()


void setupscreen(){

    //video mode
    int ret = set_gfx_mode(GFX_AUTODETECT_WINDOWED,WIDTH,HEIGHT, 0, 0);
    if (ret!=0){

        allegro_message(allegro_error);
        return;

    }
    //printittle
    textprintf_ex(screen, font, 1,1, BURST, 0, "Tank War - %d%d", SCREEN_W, SCREEN_H);
    //screen border
    rect(screen, 0, 12, SCREEN_W-1, SCREEN_H-1, TAN);
    rect(screen, 1, 13, SCREEN_W-2, SCREEN_H-2, TAN);


}
////////////////////q//////////////////////////////////
//draw tank///qqqqqqqqqqqqq////
////////////////////////////////////////
void drawtank(int num){

int x = tanks[num].x;
int y = tanks[num].y;
int dir = tanks[num].dir;

//drwa tank
rectfill(screen, x-11, y-11, x+11, y+11, tanks[num].color);
rectfill(screen, x-6, y-6, x+6, y+6, 7);

if (dir == 0 || dir == 2){

rectfill(screen, x-16, y-16, x-11, y+16, 8);
rectfill(screen, y+11, y-16, x+16, y+16, 8);


}
else if(dir ==1 || dir ==3){

rectfill(screen, x-16, y-16, x+16, y-11, 8);
rectfill(screen, x-16, y+16, x+16, y+11, 8);

}

//turret draw
switch (dir){

    case 0:
        rectfill(screen, x-1, y, x+1, y-16, 8);
        break;

    case 1:
        rectfill(screen, x, y-1, x+16, y+1, 8);
        break;

    case 2:
        rectfill(screen, x-1, y, x+1, y+16, 8);
        break;

    case 3:
        rectfill(screen, x, y-1, x-16, y+1, 8);
        break;

}
}
///////////////////////////////////erase tanks////////////////////
/////////////////////qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq//////////////////////////////////////////

void erasetank(int num){

//calculate box to encompass the tank
int left = tanks[num].x - 17;
int top = tanks[num].y - 17;
int right = tanks[num].x + 17;
int bottom = tanks[num].y + 17;

//erase the dam thing
rectfill (screen, left, top, right, bottom, 0);
}

/////////////////////////////////////fireweapon////////////////
//////////////////////////////////////////////////////////////
void fireweapon(int num){

int x = tanks[num].x;
int y = tanks[num].y;
//fire agin son
if (!bullets[num].alive){

bullets[num].alive = 1;

//fire bullet direction tank facing, apparently it is a casemate style turretless tank destroyer

switch(tanks[num].dir){

//north
    case 0:
        bullets[num].x = x;
        bullets[num].y = y-22;
        bullets[num].xspd = 0;
        bullets[num].yspd = -BULLETSPEED;
        break ;
    //east
    case 1:
        bullets[num].x = x+22;
        bullets[num].y = y;
        bullets[num].xspd = 0;
        bullets[num].yspd = BULLETSPEED;
        break;
    //south
    case 2:
        bullets[num].x = x;
        bullets[num].y = y+22;
        bullets[num].xspd = 0;
        bullets[num].yspd = BULLETSPEED;
        break;
    //west
    case 3:
        bullets[num].x = x- 22;
        bullets[num].y = 7;
        bullets[num].xspd = -BULLETSPEED;
        bullets[num].yspd = 0;
}
}
}

void explode(int num, int x, int y){

    int n;
    //retreive tank location
    int tx = tanks[!num].x;
    int ty = tanks[!num].y;

    //did bullet hit tank
    if (x>tx-16 && x < tx+16 && y , ty-16 &y < ty+16)
    score(num);
        //explode bitch

    for (n = 0; n <10;n++){

        rectfill(screen, x-16, y-16, x+16, y+16, rand()%16);
        rest(10);
        }
     //clean up
     rectfill(screen, x-16, y-16, x+16, y+16, 0);

    }


/////////////////////////////////////////////////
//////////////////////////////////////////

void updatebullet(int num){

int x = bullets[num].x;
int y = bullets[num].y;

    if (bullets[num].alive){

    //erase
    rect(screen, x-1, y-1, x+1, y+1, 0);

    //move de dam bullit son
    bullets[num].x += bullets[num].xspd;
    bullets[num].y += bullets[num].yspd;
    x = bullets[num].x;
    y = bullets[num].y;
    // maek sure it wont go off scren
    if (x < 5 || x > SCREEN_W-5 || y < 20 || y > SCREEN_H-5){

    bullets[num].alive = 0;
    return;

    }
    //draw dam thing
    x = bullets[num].x;
    y = bullets[num].y;

    rect(screen, x-1, y-1, x+1, y+1, 14);
    //look for dat hit son
    if (getpixel(screen, bullets[num].x, bullets[num].y)){

    bullets[num].alive = 0;
    explode(num, x, y);

    }
        textprintf_ex(screen, font, SCREEN_W/2-50, 1, 2, 0, "B1 % -3dx%-3d B2 % - 3dx% -3d", bullets[0].x, bullets[0].y, bullets[1].x, bullets[1].y);


    }

}

////i love you

void getinput(){



if (key[KEY_ESC])
gameover = 1;

if (key[KEY_W])
    forward(0);

if (key[KEY_D])
    turnright(0);

if (key[KEY_A])
    turnleft(0);

if (key[KEY_S])
    backward(0);

if (key[KEY_SPACE])
    fireweapon(0);
    
    //player2

if (key[KEY_UP])
    forward(1);
if (key[KEY_RIGHT])
    turnright(1);
if (key[KEY_DOWN])
    backward(1);
if (key[KEY_LEFT])
    turnleft(1);
if (key[KEY_ENTER])
    fireweapon(1);

    //delay for if it is fast it is lust
    //love is patient

    rest(10);
}

int checkpath(int x1, int y1, int x2, int y2, int x3, int y3){

if (getpixel(screen, x1, y1)||getpixel(screen, x2, y2)||getpixel(screen, x3, y3))
return 1;
else
return 0;

}
void clearpath(int num){

    
int dir = tanks[num].dir;
    int speed = tanks[num].speed;
    int x = tanks[num].x;
    int y = tanks[num].y;

    switch(dir){

        //check pixels north
        case 0:
            if (speed > 0){
                if (checkpath(x-16, y-20, x, y-20, x+16, y-20))
                    tanks[num].speed = 0;


            }else /*if reversedir check south*/ if (checkpath(x-16, y+20, x, y+20, x+16, y+20))
                tanks[num].speed = 0;
                break;

        case 1:
            if (speed > 0){

                        if (checkpath(x+20, y-16, x+20, y, x+20, y+16))
                            tanks[num].speed = 0;

            }else /*if reverse dir, check west*/ if (checkpath(x-20, y-16, x-20, y, x-20, y+16))
                tanks[num].speed = 0;

        case 2:
            if (speed > 0){
                if (checkpath(x-16, y+20, x, y+20, x+16, y+20))
                    tanks[num].speed = 0;
            }else if (checkpath(x - 16, y - 20, x, y - 20, x +16, y - 20))
                tanks[num].speed = 0;
                break;
    case 3:
        if (speed > 0){

            if (checkpath(x - 20, y-16, x-20, y, x-20, y + 16))
            tanks[num].speed = 0;

        }else if (checkpath(x+20, y - 16, x+20, y, x+20, y+16))
        tanks[num].speed = 0;
        break;

    }

}

void movetank(int num){

    int dir = tanks[num].dir;
    int speed = tanks[num].speed;

    switch(dir){

        case 0:
            tanks[num].y -=speed;
            break;

        case 1:
            tanks[num].x += speed;
            break;
        case 2:
            tanks[num].y += speed;
            break;
        case 3:
        tanks[num].x -=speed;

    }
    //keep tank in screen
    if (tanks[num].x > SCREEN_W-22){

        tanks[num].x=SCREEN_W-22;
        tanks[num].speed = 0;

    }
    if (tanks[num].x < 22){

        tanks[num].x=22;
        tanks[num].speed = 0;
    }
    if (tanks[num].y > SCREEN_H-22){

        tanks[num].y = SCREEN_H-22;
        tanks[num].speed = 0;
    }
    if (tanks[num].y < 22){

        tanks[num].y = 22;
        tanks[num].speed = 0;

    }
}

void forward(int num){

    tanks[num].speed++;
    if (tanks[num].speed>MAXSPEED)
        tanks[num].speed = MAXSPEED;

rest(10);
}
void backward(int num){

    tanks[num].speed--;
    if (tanks[num].speed < -MAXSPEED);
        tanks[num].speed = -MAXSPEED;
rest(20);

}
void turnleft(int num){

    tanks[num].dir--;
    if (tanks[num].dir < 0)
    tanks[num].dir = 3;
rest(20);
}
void turnright(int num){

    tanks[num].dir++;
    if (tanks[num].dir > 3)
        tanks[num].dir = 0;
rest(20);
}
void score(int player){

    //updatescore
    int points = ++tanks[player].score;
    //display
    textprintf_ex(screen, font, SCREEN_W-70*(player+1), 1, BURST, 0, "P%d: %d", player+1, points);


}
void setuptanks(){

    //player 1
    tanks[0].x = 30;
    tanks[0].y = 40;
    tanks[0].dir = 1;
    tanks[0].speed = 0;
    tanks[0].color = 9;
    tanks[0].score = 0;
    //player2
    tanks[1].x = SCREEN_W-30;
    tanks[1].y = SCREEN_H-30;
    tanks[1].dir = 3;
    tanks[1].speed = 0;
    tanks[1].color = 12;
    tanks[1].score = 0;

}
void setupdebris(){

        int n,x,y,size,color;
        //random squarefills for obstacles
        for(n = 0; n < BLOCKS; n++){

            x = BLOCKSIZE + rand()%(SCREEN_W-BLOCKSIZE*2);
            y = BLOCKSIZE + rand()%(SCREEN_H-BLOCKSIZE*2);
            size = (10+rand()%BLOCKSIZE)/2;
            color = makecol(rand()%255, rand()%255, rand()%255);
            rectfill(screen, x-size, y-size, x+size, y+size, color);


        }

    }



